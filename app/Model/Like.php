<?php
App::uses('AppModel', 'Model');
/**
 * Like Model
 *
 * @property Post $Post
 * @property Liker $Liker
 */
class Like extends AppModel 
{

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'post_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'liker_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    // The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Post' => array(
            'className' => 'Post',
            'foreignKey' => 'post_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Liker' => array(
            'className' => 'User',
            'foreignKey' => 'liker_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}
