<?php
App::uses('AppModel', 'Model');
/**
 * Comment Model
 *
 * @property Post $Post
 * @property User $User
 */
class Comment extends AppModel 
{

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'id';


    // The Associations below have been created with all possible keys, those that are not needed can be removed

    public $validate = array(
        'comment_text' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'insert a comment',
                //'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        
    );

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Post' => array(
            'className' => 'Post',
            'foreignKey' => 'post_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}
