<?php
    App::import('Controller', 'Users');
    $userCtrl = new UsersController();
?>
<?php if (AuthComponent::user()) : ?>
  <?php $userSession = $userCtrl->getUserDetailsById(AuthComponent::user('id')); ?>
  <div class="actions"> 
    <?php
        echo $this->Html->image(
            $userSession['User']['profile_picture'], 
            array(
                'width' => '56px',
                'height' => '56px'
            )
        ); 
    ?> 
    <h4> <?= $userSession['User']['first_name']; ?> <?= $userSession['User']['last_name']; ?>  </h4>
    <h5><b> @<?= $userSession['User']['username']; ?> <b></h5><br>
    <ul>
      <li><?= $this->HTML->link('Home', array('controller' => 'posts', 'action' => 'index')); ?></li>
      <li><?= $this->HTML->link('Search', array('controller' => 'posts', 'action' => 'search')); ?></li>
      <li>
        <?= 
            $this->HTML->link(
                'My Profile', 
                array(
                    'controller' => 'users', 
                    'action' => 'view', 
                    AuthComponent::user('id') 
                )
            ); 
        ?>
      </li>
      <li><?= $this->HTML->link('Log out', array('controller' => 'users', 'action' => 'logout')); ?></li>
    </ul>
  </div>
<?php endif; ?>

<div class="user form">
  <?php if (end($follows) == 0) : ?> <!-- Users Followed by <username> -->
    <h3>
      <div class="row">
        <div class="col-md-3">
        Users followed by 
        </div>
        <div class="col-md-3" >
        <?php foreach($follows as $follow) : ?>
          <p>@<?= $follow['Follower']['username'] ?>:</p>
          <?php break; ?>
        <?php endforeach ?>
        </div>
      </div>
    </h3>
  <?php endif; ?>
  <?php if (end($follows) == 1) : ?> <!-- Users who followed <username> -->
    <h3>
      <div class="row">
        <div class="col-md-3">
        Users who followed 
        </div>
        <div class="col-md-3" >
        <?php foreach($follows as $follow) : ?>
          <p>@<?= $follow['User']['username'] ?>:</p>
          <?php break; ?>
        <?php endforeach ?>
        </div>
      </div>
    </h3>
  <?php endif; ?>
  <div>
    <div class="container-fluid">
      <?php foreach($follows as $follow) : ?>
        <?php if (!empty($follow['Follow'])) : ?>
          <?php if ($follow['Follow']['unfollow_state'] == 0) : ?>
            <div class="panel panel-default">
              <div class="panel-body">
                <?php if (end($follows) == 0) : ?>
                  <div class="row">
                    <div class="col-md-1">
                      <img src="<?= $follow['User']['username'] ?>" width="28px" height="28px">
                    </div>
                    <div class="col-md-2">
                      <p><b><?= 
                        $this->HTML->link(
                        $follow['User']['first_name'].' '.$follow['User']['last_name'], 
                        array('controller' => 'users', 'action' => 'view', $follow['User']['id'])); 
                      ?></b></p>
                    </div>
                    <div class="col-md-3">
                      <p>@<?= $follow['User']['username'] ?></p>
                    </div>
                  </div>
                <?php endif; ?>
                <?php if (end($follows) == 1) : ?>
                  <div class="row">
                    <div class="col-md-1">
                      <img src="<?= $follow['Follower']['username'] ?>" width="28px" height="28px">
                    </div>
                    <div class="col-md-2">
                      <p><b><?= 
                        $this->HTML->link(
                        $follow['Follower']['first_name'].' '.$follow['Follower']['last_name'], 
                        array('controller' => 'users', 'action' => 'view', $follow['Follower']['id'])); 
                      ?></b></p>
                    </div>
                    <div class="col-md-3">
                      <p>@<?= $follow['Follower']['username'] ?></p>
                    </div>
                  </div>
                <?php endif; ?>
              </div>
            </div>
          <?php endif; ?>
        <?php endif; ?>
      <?php endforeach; ?>
      <?php unset($follow); ?>
    </div>
  </div>
</div>
