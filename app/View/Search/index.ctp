<?php
    App::import('Controller', 'Likes');
    App::import('Controller', 'Retweets');
    App::import('Controller', 'Comments');
    App::import('Controller', 'Posts');
    App::import('Controller', 'Users');

    $likeCtrl = new LikesController();
    $retweetCtrl = new RetweetsController();
    $commentCtrl = new CommentsController();
    $postCtrl = new PostsController();
    $userCtrl = new UsersController();
?>

<?php if (AuthComponent::user()) : ?>
  <?php $userSession = $userCtrl->getUserDetailsById(AuthComponent::user('id')); ?>
  <div class="actions"> 
    <?php
        echo $this->Html->image(
          $userSession['User']['profile_picture'], 
          array(
                'width' => '56px',
                'height' => '56px'
          )
        ); 
    ?> 
    <h4> <?= $userSession['User']['first_name']; ?> <?= $userSession['User']['last_name']; ?>  </h4>
    <h5><b> @<?= $userSession['User']['username']; ?> <b></h5><br>
    <ul>
      <li><?= $this->HTML->link('Home', array('controller' => 'posts', 'action' => 'index')); ?></li>
      <li>
        <?= 
            $this->HTML->link(
                'My Profile', 
                array(
                    'controller' => 'users', 
                    'action' => 'view', 
                    AuthComponent::user('id')
                )
            ); 
        ?>
      </li>
      <li><?= $this->HTML->link('Log out', array('controller' => 'users', 'action' => 'logout')); ?></li>
    </ul>
  </div>
<?php endif; ?>

<div class="users form">
  <form method="post" role="search" action="/Search/index">
    <div class="form-group">
      <div class="row">
          <div class="input-group">
            <input name="details" id="details" type="text" value=<?= $detail['details'] ?> class="form-control" placeholder="Search username or posts">
            <span class="input-group-btn">
              <button class="btn btn-default" type="submit">Search</button>
            </span>
          </div>
      </div>
    </div>
  </form>

  <?php if (!empty($detail)) : ?>
      <?php $posts = $postCtrl->retrievePossiblePostsByText($detail); ?>
      <?php $users = $userCtrl->retrievePossibleUsersByUsername($detail); ?> 
  <?php endif; ?>
  <ul class="nav nav-tabs nav-justified">

    <li id="generalTab" role="presentation" class="active"><a href="#">All</a></li>
    <li id="userTab" role="presentation"><a href="#">People</a></li>
    <li id="postTab" role="presentation"><a href="#">Posts</a></li>

  </ul>

  <div id="generalPanel" class="container-fluid">

    <?php if (!empty($users) && !empty($posts)) : ?>
        <?php include '../View/Search/userList.ctp'; ?>
        <?php include '../View/Search/postList.ctp'; ?>
    <?php endif; ?>
    
    <?php if (!empty($users) && empty($posts)) : ?>
        <?php include '../View/Search/userList.ctp'; ?>
    <?php endif; ?>
    
    <?php if (empty($users) && !empty($posts)) : ?>
        <?php include '../View/Search/postList.ctp'; ?>
    <?php endif; ?>
    
    <?php if (empty($users) && empty($posts)) : ?>
        <h3 class="text-center"> No results for '<?= $detail['details'] ?>'. </h3>
    <?php endif; ?>

  </div>

  <div id="usersPanel" class="container-fluid">
    <?php if (!empty($users)) : ?>
        <?php include '../View/Search/userList.ctp'; ?>
    <?php else : ?>
        <h3 class="text-center"> No user named '<?= $detail['details'] ?>'. </h3>
    <?php endif; ?>
  </div>

  <div id="postsPanel" class="container-fluid">
    <?php if (!empty($posts)) : ?>
        <?php include '../View/Search/postList.ctp'; ?>
    <?php else : ?>
        <h3 class="text-center"> No post associated to '<?= $detail['details'] ?>'. </h3>
    <?php endif; ?>
  </div>

</div>
<script type="text/javascript" src="../js/searchtab.js"></script>
