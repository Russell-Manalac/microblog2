<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">People </h3>
  </div>
  <div class="panel-body">
    <?php foreach($users as $user) : ?>
      <?php if (!empty($user['User'])) : ?>
        <?php if ($user['User']['disable_state'] == 0) : ?>
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="row">
                <div class="col-md-1">
                  <img src="<?= $user['User']['profile_picture'] ?>" width="28px" height="28px">
                </div>
                <div class="col-md-2">
                  <p><b>
                <?= 
                    $this->HTML->link(
                        $user['User']['first_name'].' '.$user['User']['last_name'], 
                        array(
                            'controller' => 'User', 
                            'action' => 'view', 
                            $user['User']['id']
                        )
                    ); 
                ?>
                </b></p>
                </div>
                <div class="col-md-3">
                  <p>@<?= $user['User']['username'] ?></p>
                </div>
              </div>
            </div>
          </div>
        <?php endif; ?>
      <?php endif; ?>
    <?php endforeach; ?>
  </div>
</div>