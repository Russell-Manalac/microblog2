<?php
$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version());
?>
<!DOCTYPE html>
<html>
  <head>
    <?php echo $this->Html->charset(); ?>
    <title>
      <?php //echo $cakeDescription ?>
      <?php echo "MicroBlog-Mañalac v1.0" ?>
    </title>
    <?php
      echo $this->Html->meta('icon');
      echo $this->Html->css('cake.generic');
      echo $this->fetch('meta');
      echo $this->Html->css('bootstrap.min');
      //echo $this->Html->css('custom');
      //echo $this->fetch('css');
      //echo $this->fetch('script');
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  </head>
<body>
<div id="container">
	<div>
	<nav class="navbar navbar-default" style="background-color: #99CCFF;">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		<h3><a class="navbar-brand" href="#"><?php echo $this->Html->link('MicroBlog-Manalac', 'http://localhost/'); ?></a></h3>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		
		<ul class="nav navbar-nav navbar-right">
			<?php if (AuthComponent::user()) : ?><!--
				<form class="navbar-form navbar-left" method="post" role="search" action="/Search/index">
					<div class="form-group">
						<input name="details" id="details" type="text" class="form-control" placeholder="Search">
					</div>
					<button type="submit" class="btn btn-default">Search</button>
				</form>
				-->
				<!--
				<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><?php// $this->HTML->link('View my Profile', array('controller' => 'users', 'action' => 'view', AuthComponent::user('id'))); ?></li>
					<li role="separator" class="divider"></li>
					<li><?php// $this->HTML->link('Log out', array('controller' => 'users', 'action' => 'logout')); ?></li>
				</ul>
				-->
			<?php else: ?>

				<!--<li><?php// $this->HTML->link('Log in', array('controller' => 'users', 'action' => 'login')); ?></li>-->

			<?php endif ?>
			</li>
		</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
	</nav>
	</div>
	<div id="content">

		<?php echo $this->Flash->render(); ?>

		<?php echo $this->fetch('content'); ?>

	</div><!--
	<div>
		<nav class="navbar navbar-default" style="background-color: #99CCFF; height: 100%; width: 100%; bottom: 0;">
		<?php/* echo $this->Html->link(
				$this->Html->image('cake.power.gif', array('alt' => $cakeDescription, 'border' => '0')),
				'https://cakephp.org/',
				array('target' => '_blank', 'escape' => false, 'id' => 'cake-powered')
			);*/
		?>
		<p>
			<?php //echo $cakeVersion; ?>
		</p>
		</nav>
	</div> -->
</div>
<?php //echo $this->element('sql_dump'); ?>
</body>
</html>
