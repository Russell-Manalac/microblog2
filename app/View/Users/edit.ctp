
<?php
  App::import('Controller', 'Users');

  $userCtrl = new UsersController; 
?>

<div class="actions">
  <?php if (AuthComponent::user()) : ?>
    <?php $userSession = $userCtrl->getUserDetailsById(AuthComponent::user('id')); ?>
    <div class="actions"> 
      <?php
          echo $this->Html->image(
              $userSession['User']['profile_picture'], 
              array(
                  'width' => '56px',
                  'height' => '56px'
              )
          ); 
      ?> 
      <h4> <?= $userSession['User']['first_name']; ?> <?= $userSession['User']['last_name']; ?>  </h4>
      <h5><b> @<?= $userSession['User']['username']; ?> <b></h5><br>
      <ul>
        <li><?= $this->HTML->link('Home', array('controller' => 'posts', 'action' => 'index')); ?></li>
        <li>
          <?php 
              echo $this->HTML->link(
                  'My Profile', 
                  array(
                      'controller' => 'users', 
                      'action' => 'view', 
                      AuthComponent::user('id')
                  )
              ); 
          ?>
        </li>
        <li><?= $this->HTML->link('Log out', array('controller' => 'users', 'action' => 'logout')); ?></li>
      </ul>
    </div>
  <?php endif; ?>
</div>
<div class="users form">
  <?php echo $this->Form->create('User', array('enctype'=>'multipart/form-data')); ?>
      <fieldset>
          <legend><?php echo __('Edit User Details'); ?></legend>
      <b> Profile Picture </b><br>
      <?php
          echo $this->Html->image($this->request->data['User']['profile_picture'], array(
                  'width' => '128px',
                  'height' => '128px'
              )
          ); 
      ?> 
      <?php
          echo $this->Form->file('profile_picture');
          echo $this->Form->input('id');
          echo $this->Form->input('username');
          echo $this->Form->input('first_name');
          echo $this->Form->input('last_name');
      ?>
      <b> Birth date </b>
      <?php	
          echo $this->Form->date(
              'birth_date',
              array(
                  'label' => 'Date of birth'
              )
          );
          echo $this->Form->input('email');
      ?>
      </fieldset>
  <?php echo $this->Form->end(__('Update Details')); ?>

  <fieldset>
    <legend><?php echo __('Update Password'); ?></legend>
    <?php
        echo $this->Form->create(
            'User', 
            array(
                    'url' => array(
                        'Controller' => 'users',
                        'action' => 'editPassword',
                        2,
                        $this->request->data['User']['id']
                )
            )
        );
        echo $this->Form->input(
            'old_Password',
            array(
                'type' => 'password'
            )
        );
        echo $this->Form->input(
            'new_Password',
            array(
                'type' => 'password'
            )
        );
        echo $this->Form->input(
            'confirm_Password',
            array(
                'type' => 'password'
            )
        );
        echo $this->Form->end('Update Password');
    ?>
  </fieldset>
</div>
