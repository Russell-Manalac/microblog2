<div class="users form">
<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php echo __('Add User'); ?></legend>
    <?php
        echo $this->Form->input('username');
        echo $this->Form->input('first_name');
        echo $this->Form->input('last_name');
    ?>
    <b> Birth date </b>
    <?php
        echo $this->Form->date(
            'birth_date', 
            array(
                'data-date-format' => 'yyyy-mm-dd'
            )
        );
        echo $this->Form->input('password');
        echo $this->Form->input('email');
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
