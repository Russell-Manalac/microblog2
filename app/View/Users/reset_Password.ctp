<div class="users form">
<?php 
    echo $this->Form->create(
        'User',
        array(
            'url' => array(
                'Controller' => 'users',
                'action' => 'renewPassword',
                1,
                $userDetails['User']['activation_code'],
                $userDetails['User']['id']
            )
        )
    ); 
?>
    <fieldset>
        <legend><?php echo __('Reset Password'); ?></legend>
    <?php
        echo $this->Form->input(
            'new_Password',
            array(
                'type' => 'password'
            )
        );
        echo $this->Form->input(
            'confirm_Password',
            array(
                'type' => 'password'
            )
        );
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Confirm new Password')); ?>
</div>
