<h1>Log in</h1>

<?php

    echo $this->Form->create('User');
    echo $this->Form->input('username');
    echo $this->Form->input('password');
    echo $this->Html->link(__('Forgot Password?'), array('action' => 'sendResetRequest'));
    echo $this->Form->end('Log in');
    echo $this->Html->link(
        __('Register'), 
        array(
            'action' => 'add'
        ),
        array(
            'class' => 'btn btn-primary'
        )
    );

?>