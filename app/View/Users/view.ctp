<!-- Profile head -->
<?php
    App::import('Controller', 'Likes');
    App::import('Controller', 'Retweets');
    App::import('Controller', 'Comments');
    App::import('Controller', 'Posts');
    App::import('Controller', 'Users');

    $likeCtrl = new LikesController();
    $retweetCtrl = new RetweetsController();
    $commentCtrl = new CommentsController();
    $postCtrl = new PostsController();
    $userCtrl = new UsersController();
?>
<?php if (AuthComponent::user()) : ?>
  <?php $userSession = $userCtrl->getUserDetailsById(AuthComponent::user('id')); ?>
  <div class="actions"> 
    <?php
        echo $this->Html->image(
          $userSession['User']['profile_picture'], 
          array(
                'width' => '56px',
                'height' => '56px'
          )
        ); 
    ?> 
    <h4> <?= $userSession['User']['first_name']; ?> <?= $userSession['User']['last_name']; ?>  </h4>
    <h5><b> @<?= $userSession['User']['username']; ?> <b></h5><br>
    <ul>
      <li><?= $this->HTML->link('Home', array('controller' => 'posts', 'action' => 'index')); ?></li>
      <li>
        <?= 
            $this->HTML->link(
                'My Profile', 
                array(
                    'controller' => 'users', 
                    'action' => 'view', 
                    AuthComponent::user('id')
                )
            ); 
        ?>
      </li>
      <li><?= $this->HTML->link('Log out', array('controller' => 'users', 'action' => 'logout')); ?></li>
    </ul>
  </div>
<?php endif; ?>

<?php if ($user['User']['disable_state'] == 0) : ?>
  <div class="users form">
    <div class="container-fluid">
      <?php $is_included = 0; ?>
      <!-- Profile Picture, Full name, and username -->
      <div class="col-md-10">
        <div class="row">
          <div class="col-md-2">
            <?php
                echo $this->Html->image($user['User']['profile_picture'], array(
                        'width' => '100px',
                        'height' => '100px'
                    )
                ); 
            ?> 
          </div>
          <div class="col-md-7">
          <h3><b><?= h($user['User']['first_name']); ?> <?= h($user['User']['last_name']); ?></b></h3>
          <p>@<?= $user['User']['username'] ?>	</p>
          </div>
        </div>
        <!-- Birthday and Email -->
        <br>
        <div class="row">
          <div class="col-md-3">
            <p>
              <b><?= $user[0] //count of followed users ?></b> 
              <?= $this->Html->link(
                  __('Following'), 
                  array(
                      'controller' => 'follows', 
                      'action' => 'view', 
                      $user['User']['id'], 
                      0/*0 = Display Following*/
                  )
              );
              ?>
            </p>
          </div>
          <div class="col-md-3">
            <p>
            <b><?= $user[1] //count of followers ?></b> 
            <?= 
                $this->Html->link(
                    __('Followers'), 
                    array(
                        'controller' => 'follows', 
                        'action' => 'view', 
                        $user['User']['id'], 
                        1/*1 = Display Followers*/
                    )
                ); 
            ?>
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
          <p><b>Birth Date: </b><?= $user['User']['birth_date']; ?></p>
          </div>
          <div class="col-md-5">
          <p><b>Email: </b><?= $user['User']['email']; ?></p>
          </div>
        </div>
      </div>
      <div class="col-md-1">
      <?php if (AuthComponent::user('id') == $user['User']['id']) :?>
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
          <span class="glyphicon glyphicon-option-horizontal" aria-hidden="true"></span>
          </button>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
            <li>
            <?php 
                echo $this->Form->postLink(
                    __('Disable Account'), 
                    array(
                        'action' => 'delete', 
                        $user['User']['id']
                    ), 
                    array(
                        'confirm' => __('Are you sure you want to disable your account?', 
                        $user['User']['id'])
                    )
                ); 
            ?> 
            </li>
          </ul> 
      </div>
      <?php else : ?>
        <ul>
          <?php if (!empty($user['Follow'])) : ?>
            <?php foreach ($user['Follow'] as $follow) : ?>
              <?php if ($follow['unfollow_state'] == 0) : ?>
                <?php if ($follow['follower_id'] == AuthComponent::user('id')) : ?>
                  <li>
                  <?php 
                      echo $this->Form->postLink(
                          __('Unfollow'), 
                          array(
                              'controller' => 'follows', 
                              'action' => 'delete', 
                              $user['User']['id'], 
                              AuthComponent::user('id'), 
                              $user['User']['username']
                          )
                      ); 
                  ?>
                  </li>
                  <?php
                    $is_included = 1;
                    break; 
                  ?>
                <?php endif; ?>
              <?php else : ?>
                <?php if ($follow['follower_id'] == AuthComponent::user('id')) : ?>
                  <li>
                  <?php 
                      echo $this->Form->postLink(
                          __('Follow'), 
                          array(
                              'controller' => 'follows', 
                              'action' => 'add', 
                              $user['User']['id'], 
                              AuthComponent::user('id'), 
                              $user['User']['username']
                          )
                      ); 
                  ?> 
                  </li>
                  <?php
                    $is_included = 1;
                    break; 
                  ?>
                <?php endif; ?>
              <?php endif; ?>
            <?php endforeach; ?>
            <?php if ($is_included == 0) : ?>
              <li>
                <?php 
                    echo $this->Form->postLink(
                        __('Follow'), 
                        array(
                            'controller' => 'follows', 
                            'action' => 'add', 
                            $user['User']['id'], 
                            AuthComponent::user('id'),
                            $user['User']['username']
                        )
                    ); 
                ?> 
              </li>
            <?php endif; ?>
          <?php else : ?>
            <li>
              <?php 
                  echo $this->Form->postLink(
                      __('Follow'), 
                      array(
                          'controller' => 'follows', 
                          'action' => 'add', 
                          $user['User']['id'], 
                          AuthComponent::user('id'), 
                          $user['User']['username']
                      )
                  ); 
              ?> 
            </li>
          <?php endif; ?>
        </ul>
      <?php endif; ?>
      </div>
    </div>

    <!-- Posts -->
    <div class="related">
      <h3><?php echo __('Posts'); ?></h3>
      <?php if (!empty($user['Post'])): ?>
      <?php endif; ?>

      <div class="container-fluid">

        <?php $retweets = $retweetCtrl->getRetweetsById($user['User']['id']); ?>
        <?php $lastArr = end($user['Post']); ?>
        <?php $arrPosts = array(); ?>
  
        <?php foreach($user['Post'] as $post) : ?>
          <?php if ($post == $lastArr) : ?>
              <?php array_push($post, $post['created']); ?>
              <?php array_push($post, 0); ?>
              <?php array_push($arrPosts, $post); ?>
              <?php foreach($retweets as $retweet) : ?>
                <?php array_push($retweet, $retweet['Retweet']['created']); ?>
                <?php array_push($arrPosts, $retweet); ?>
              <?php endforeach; ?>
              <?php break; ?>
          <?php else : ?>
              <?php array_push($post, $post['created']); ?>
              <?php array_push($arrPosts, $post); ?>
          <?php endif; ?>
        <?php endforeach; ?>
        <?php
            usort($arrPosts, function($a, $b) {
                $a = strtotime($a["0"]);
                $b = strtotime($b["0"]);
                return $b - $a;
            });
        ?>
        <?php foreach ($arrPosts as $post) : ?>
        
          <?php if (!empty($post['Retweet'])) : ?>
            <?php include '../View/Users/userRepostPanel.ctp'; ?>
          <?php else : ?>
            <?php include '../View/Users/userPostPanel.ctp'; ?>
          <?php endif; ?>

        <?php endforeach; ?>
        <?php unset($post); ?>
      </div>
    </div>
  </div>
<?php else : ?>
    <?php include '../View/Users/deactivated.ctp'; ?>
<?php endif; ?>
