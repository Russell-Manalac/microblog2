<!-- $retweet['Retweet'][]  -->
<?php $postDetails = $postCtrl->viewRetweetedPostById($post['Post']['id']); ?>
<?php if ($retweet['Retweet']['delete_state'] == 0) : ?>
  <?php if (AuthComponent::user('id') == $post['User']['id']) : ?>
    <p><b><i> 
      You Reposted
    </i></b></p>
    <div class="panel panel-warning">
  <?php endif; ?>

  <?php if (AuthComponent::user('id') != $post['User']['id']) : ?>
    <p><b><i> 
      <?= $post['User']['username'] ?> Reposted
    </i></b></p>
    <div class="panel panel-info">
  <?php endif; ?>

  <div class="panel-heading">
    <div class="row">
      <div class="col-sm-1">
        <?php
            echo $this->Html->image(
                $postDetails['User']['profile_picture'], 
                array(
                    'width' => '28px',
                    'height' => '28px'
                )
            ); 
        ?> 
      </div>
      <div class="col-sm-2">
        <h5>
          <?= 
              $this->HTML->link(
                  $postDetails['User']['first_name'].' '.$postDetails['User']['last_name'], 
                  array(
                      'controller' => 'users', 
                      'action' => 'view', 
                      $postDetails['User']['id']
                  )
              );
          ?>
        </h5>
      </div>
      <div class="col-sm-8">
        <h5><b>@<?= $postDetails['User']['username'] ?></b></h5>
      </div>
      <div class="col-sm-1">
        <?php if(AuthComponent::user('id') == $post['User']['id']) : ?>
          <div class="dropdown">
              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              <span class="glyphicon glyphicon-option-horizontal" aria-hidden="true"></span>
              </button>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
              <li>
              <?php
                  echo $this->Form->postLink(
                      'Delete', 
                      array(
                          'controller' => 'retweets', 
                          'action' => 'delete', 
                          $post['Retweet']['id'],
                          $post['Retweet']['post_id'],
                          $post['Retweet']['user_id'],
                          3
                      ), 
                      array(
                          'confirm' => 'Are you sure you want to delete this repost?'
                      )
                  ); 
              ?>
              </li>
              </ul>
          </div>
        <?php endif ?>
      </div>
    </div>
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-sm-10">
        <p><?= $post['Post']['post_text'] ?></p>
      </div>
      </div><br>
      <div class="row">
      <div class="col-sm-6">
        Time posted: <?= $post['Post']['created']; ?>
      </div>
    </div>
  </div>
  <div class="panel-footer">
      <div class="row">
      <div class="col-sm-4">
        <p class="text-left"><?= $likeCtrl->getCurrentLikesById($post['Post']['id']); ?> Likes</p>
      </div>
      <div class="col-sm-4">
        <p class="text-center"><?= $commentCtrl->getCommentCountById($post['Post']['id']); ?> Comments</p>
      </div>
      <div class="col-sm-4">
        <p class="text-right"><?= $retweetCtrl->getRetweetCountById($post['Post']['id']); ?> Reposts</p>
      </div>
      </div>
  </div>
  <div class="panel-footer">
      <div class="row">
        <p class="text-center">
          <?= 
              $this->HTML->link(
                  'View Post', 
                  array(
                      'controller' => 'posts', 
                      'action' => 'view', 
                      $post['Post']['id']
                  )
              ); 
          ?>
        </p>
      </div>
  </div>
</div>
<?php endif; ?>