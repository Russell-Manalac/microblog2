<div class="users form">
<?php 
    echo $this->Form->create(
        'User',
        array(
            'url' => array(
                'Controller' => 'users',
                'action' => 'sendResetRequest'
            )
        )
    );
?>
    <fieldset>
        <legend><?php echo __('Password reset request'); ?></legend>
        <b>kindly enter the following details for password reset request</b>
    <?php
          echo $this->Form->input('username');
          echo $this->Form->input('email');
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Confirm Request')); ?>
</div>