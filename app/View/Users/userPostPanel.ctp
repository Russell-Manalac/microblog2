<?php if ($post['delete_state'] == 0) : ?>
<div class="panel panel-success">
  <div class="panel-heading">
    <div class="row">
      <div class="col-sm-1">
        <?php
            echo $this->Html->image($user['User']['profile_picture'], array(
                    'width' => '28px',
                    'height' => '28px'
                )
            ); 
        ?>
      </div>
      <div class="col-sm-2">
        <h5><?= $user['User']['first_name']; ?> <?= $user['User']['last_name']; ?></h5>
      </div>
      <div class="col-sm-8">
        <h5><b>@<?= $user['User']['username'] ?></b></h5>
      </div>
      <div class="col-sm-1">
        <?php if(AuthComponent::user('id') == $post['user_id']) : ?>
          <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              <span class="glyphicon glyphicon-option-horizontal" aria-hidden="true"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
              <li>
                <?= 
                  $this->HTML->link('
                      Edit', 
                      array(
                          'controller' => 'posts', 
                          'action' => 'edit', 
                          $post['id']
                      )
                  ); 
                ?>
              </li>
              <li>
              <?php 
                  echo $this->Form->postLink(
                      'Delete', 
                      array(
                          'controller' => 'posts', 
                          'action' => 'delete', 
                          $post['id'],
                          $post['user_id'],
                          3
                      ), 
                      array(
                          'confirm' => 'Are you sure you want to delete this post?'
                      )
                  ); 
              ?>
              </li>
            </ul>
          </div>
        <?php endif ?>
      </div>
    </div>
  </div>

  <div class="panel-body">
    <div class="row">
      <div class="col-sm-10">
        <p><?= $post['post_text'] ?></p>
      </div>
    </div><br>
    <div class="row">
      <div class="col-sm-6">
        Time posted: <?= $post['created']; ?>
      </div>
    </div>
  </div>

  <div class="panel-footer">
    <div class="row">
      <div class="col-sm-4">
        <p class="text-left"><?= $likeCtrl->getCurrentLikesById($post['id']); ?> Likes</p>
      </div>
      <div class="col-sm-4">
        <p class="text-center"><?= $commentCtrl->getCommentCountById($post['id']); ?> Comments</p>
      </div>
      <div class="col-sm-4">
        <p class="text-right"><?= $retweetCtrl->getRetweetCountById($post['id']); ?> Reposts</p>
      </div>
    </div>
  </div>

  <div class="panel-footer">
    <div class="row">
        <p class="text-center">
          <?= 
              $this->HTML->link(
                  'View Post', 
                  array(
                      'controller' => 'posts', 
                      'action' => 'view', 
                      $post['id']
                  )
              ); 
          ?>
        </p>
    </div>
  </div>
</div>
<?php endif ?>