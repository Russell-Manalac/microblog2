

<?php
  App::import('Controller', 'Users');
  App::import('Controller', 'Likes');
  App::import('Controller', 'Retweets');
  App::import('Controller', 'Comments');
  App::import('Controller', 'Post');

  $postCtrl = new PostsController();
  $likeCtrl = new LikesController();
  $retweetCtrl = new RetweetsController();
  $commentCtrl = new CommentsController();
  $userCtrl = new UsersController; 
?>
<?php $is_included = 0; ?>

<?php if (AuthComponent::user()) : ?>
  <?php $userSession = $userCtrl->getUserDetailsById(AuthComponent::user('id')); ?>
  <div class="actions"> 
    <?php
        echo $this->Html->image(
            $userSession['User']['profile_picture'], 
            array(
                'width' => '56px',
                'height' => '56px'
            )
        ); 
    ?> 
    <h4> <?= $userSession['User']['first_name']; ?> <?= $userSession['User']['last_name']; ?>  </h4>
    <h5><b> @<?= $userSession['User']['username']; ?> <b></h5><br>
    <ul>
      <li><?= $this->HTML->link('Home', array('controller' => 'posts', 'action' => 'index')); ?></li>
      <li>
        <?= 
            $this->HTML->link(
                'My Profile', 
                array(
                    'controller' => 'users', 
                    'action' => 'view', 
                    AuthComponent::user('id') 
                )
            ); 
        ?>
      </li>
      <li><?= $this->HTML->link('Log out', array('controller' => 'users', 'action' => 'logout')); ?></li>
    </ul>
  </div>
<?php endif; ?>

<?php if (!empty($comments['Post'])) : ?>
  <?php $postDetails = $comments ?>
<?php endif; ?>

<?php if (empty($comments['Post'])) : ?>
  <?php $postDetails = $postCtrl->getPostDetailsById($comments[0]); ?>
<?php endif; ?>

<?php if ($postDetails['User']['disable_state'] == 0) : ?>
  <div class="users form"> 
    <div class="panel panel-default">

      <div class="panel-heading">
        <div class="row">
          <div class="col-sm-1">
            <?php
                echo $this->Html->image($postDetails['User']['profile_picture'], array(
                        'width' => '28px',
                        'height' => '28px'
                    )
                ); 
            ?> 
          </div>
          <div class="col-sm-2">
            <h5>
              <?= 
                  $this->HTML->link(
                      $postDetails['User']['first_name'].' '.$postDetails['User']['last_name'], 
                      array(
                          'controller' => 'users', 
                          'action' => 'view', 
                          $postDetails['User']['id']
                      )
                  );
              ?>
            </h5>
          </div>
          <div class="col-sm-8">
            <h5><b>@<?= $postDetails['User']['username'] ?></b></h5>
          </div>
          <div class="col-sm-1">
            <?php if(AuthComponent::user('id') == $postDetails['Post']['user_id']) : ?>
              <div class="dropdown">
                  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                  <span class="glyphicon glyphicon-option-horizontal" aria-hidden="true"></span>
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                  <li>
                    <?= 
                        $this->HTML->link(
                            'Edit', 
                            array(
                                'controller' => 'posts', 
                                'action' => 'edit', 
                                $postDetails['Post']['id'],
                                $postDetails['User']['id'],
                                1
                            )
                        ); 
                    ?>
                  </li>
                  <li>
                    <?php 
                        echo $this->Form->postLink(
                            'Delete', 
                            array(
                                'controller' => 'posts', 
                                'action' => 'delete', 
                                $postDetails['Post']['id'],
                                $postDetails['User']['id'],
                                1
                            ), 
                            array(
                                'confirm' => 'Are you sure you want to delete this post?'
                            )
                        ); 
                    ?>
                  </li>
                  </ul>
              </div>
            <?php endif ?>
          </div>
        </div>
      </div>

      <div class="panel-body">
        <div class="row">
          <div class="col-sm-10">
            <h1><?= $postDetails['Post']['post_text'] ?></h1>
          </div>
        </div><br>
        <div class="row">
          <div class="col-sm-6">
            Time posted: <?= $postDetails['Post']['created']; ?>
          </div>
        </div>
      </div>
      <div class="panel-footer">
        <div class="row">
          <div class="col-sm-4">
            <p class="text-left">
              <?= $likeCtrl->getCurrentLikesById($postDetails['Post']['id']); ?> 
              <?= 
                  $this->HTML->link('Likes', 
                      array(
                          'controller' => 'likes',
                          'action' => 'view',
                          $postDetails['Post']['id']
                      )
                  ); 
              ?>
            </p>
          </div>
          <div class="col-sm-4">
            <p class="text-center"><?= $commentCtrl->getCommentCountById($postDetails['Post']['id']); ?> Comments</p>
          </div>
          <div class="col-sm-4">
            <p class="text-right">
              <?= $retweetCtrl->getRetweetCountById($postDetails['Post']['id']); ?>
              <?= 
                  $this->HTML->link('Reposts', 
                      array(
                          'controller' => 'retweets',
                          'action' => 'view',
                          $postDetails['Post']['id']
                      )
                  ); 
              ?>
            </p>
          </div>
        </div>
      </div>
      <div class="panel-footer">
        <div class="row">
          <div class="col-sm-6">
            <p class="text-center">
              <?php if (!empty($postDetails['Like'])) : ?>
                <?php foreach ($postDetails['Like'] as $like) : ?>
                  <?php if ($like['unlike_state'] == 0) : ?>
                    <?php if ($like['liker_id'] == AuthComponent::user('id')) : ?>
                      <?php 
                          echo $this->HTML->link(
                              'Unlike', 
                              array(
                                  'controller' => 'likes', 
                                  'action' => 'delete', 
                                  $postDetails['Post']['id'], 
                                  AuthComponent::user('id')
                              )
                          ); 
                      ?>
                      <?php
                        $is_included = 1;
                        break; 
                      ?>
                    <?php endif; ?>
                  <?php else : ?>
                    <?php if ($like['liker_id'] == AuthComponent::user('id')) : ?>
                      <?= 
                          $this->HTML->link(
                              'Like', 
                              array(
                                  'controller' => 'likes',
                                  'action' => 'add',
                                  $postDetails['Post']['id'],
                                  AuthComponent::user('id')
                              )
                          ); 
                      ?>
                      <?php
                        $is_included = 1;
                        break; 
                      ?>
                    <?php endif; ?>
                  <?php endif; ?>
                <?php endforeach; ?>
                <?php if ($is_included == 0) : ?>
                    <?= 
                        $this->HTML->link(
                            'Like', 
                            array(
                                'controller' => 'likes',
                                'action' => 'add',
                                $postDetails['Post']['id'],
                                AuthComponent::user('id')
                            )
                        ); 
                    ?>
                <?php endif; ?>
              <?php else : ?>
                  <?= 
                      $this->HTML->link(
                          'Like', 
                          array(
                              'controller' => 'likes',
                              'action' => 'add',
                              $postDetails['Post']['id'],
                              AuthComponent::user('id')
                          )
                      ); 
                  ?>
              <?php endif; ?>
            </p>
          </div>
          <div class="col-sm-5">
            <p class="text-center">
              <?= 
                  $this->HTML->link(
                      'Repost', 
                      array(
                          'controller' => 'retweets',
                          'action' => 'add',
                          $postDetails['Post']['id'],
                          AuthComponent::user('id')
                      )
                  ); 
              ?>
            </p>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div>
          <?php
              echo $this->Form->create(
                  'Comment', 
                  array(
                      'url' => array(
                          'Controller' => 'comments',
                          'action' => 'add',
                          $postDetails['Post']['id'],
                          AuthComponent::user('id')
                      )
                  )
              );
              echo $this->Form->input(
                  'comment_text',
                  array(
                      'required' => false,
                      'style' => 'resize: none;',
                      'class' => 'form-control',
                      'label' => '',
                      'placeholder' => 'Write your comment here'
                  )
              ); 
              echo $this->Form->end('Comment');
          ?>
        </div>

        <!-- COMMENTS LIST -->
        <h3> Comments: </h3>
        <?php if (!empty($comments['Post'])) : ?>
          <h4 class="text-center"> No comments yet. </h4>
        <?php endif; ?>
        <?php if (empty($comments['Post'])) : ?>

          <?php rsort($comments); ?>

          <?php foreach($comments as $comment) : ?>
            <?php 
                $user = $userCtrl->getUserDetailsById($comment['Comment']['user_id']);
            ?>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-sm-1">
                    <?php
                        echo $this->Html->image($user['User']['profile_picture'], array(
                                'width' => '28px',
                                'height' => '28px'
                            )
                        ); 
                    ?> 
                  </div>
                  <div class="col-sm-2">
                    <h5>
                      <?= 
                          $this->HTML->link(
                              $user['User']['first_name'].' '.$user['User']['last_name'], 
                              array(
                                  'controller' => 'users', 
                                  'action' => 'view', 
                                  $user['User']['id']
                              )
                          );
                      ?>
                    </h5>
                  </div>
                  <div class="col-sm-8">
                    <h5><b>@<?= $user['User']['username'] ?></b></h5>
                  </div>
                  <div class="col-sm-1">
                    <?php if(AuthComponent::user('id') == $user['User']['id']) : ?>
                      <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                          <span class="glyphicon glyphicon-option-horizontal" aria-hidden="true"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                          <li>
                            <?= 
                              $this->HTML->link('
                                  Edit', 
                                  array(
                                      'controller' => 'comments', 
                                      'action' => 'edit', 
                                      $comment['Comment']['id'],
                                      $comment['Comment']['post_id'],
                                      $user['User']['id']
                                  )
                              ); 
                            ?>
                          </li>
                          <li>
                          <?php 
                              echo $this->Form->postLink(
                                  'Delete', 
                                  array(
                                      'controller' => 'comments', 
                                      'action' => 'delete', 
                                      $comment['Comment']['id'],
                                      $comment['Comment']['post_id'],
                                      $user['User']['id']
                                  ), 
                                  array(
                                      'confirm' => 'Are you sure you want to delete this comment?'
                                  )
                              ); 
                          ?>
                          </li>
                        </ul>
                      </div>
                    <?php endif ?>
                  </div>
                </div>
              </div>

              <div class="panel-body">
                <div class="row">
                  <div class="col-sm-10">
                    <?= $comment['Comment']['comment_text']; ?>
                  </div>
                </div><br>
                <div class="row">
                  <div class="col-sm-10">
                    Commented at: <?= $comment['Comment']['created']; ?>
                  </div>
                </div>
              </div>
            </div> 
          <?php endforeach; ?>

          <?php if (!empty($comments)) : ?>
            <div class="text-center">
              <?php
                  echo $this->Paginator->counter(array(
                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                  ));
              ?>
              <div class="paging">
                <?php
                    echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                    echo $this->Paginator->numbers(array('separator' => ''));
                    echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                ?>
              </div>
            </div>
          <?php endif; ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php else : ?>
    <?php include '../View/Posts/deactivated.ctp'; ?>
<?php endif; ?>