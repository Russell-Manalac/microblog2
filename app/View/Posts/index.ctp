<?php
    App::import('Controller', 'Likes');
    App::import('Controller', 'Retweets');
    App::import('Controller', 'Comments');
    App::import('Controller', 'Posts');
    App::import('Controller', 'Users');

    $likeCtrl = new LikesController();
    $retweetCtrl = new RetweetsController();
    $commentCtrl = new CommentsController();
    $postCtrl = new PostsController();
    $userCtrl = new UsersController();
?>
<?php if (AuthComponent::user()) : ?>
  <?php $userSession = $userCtrl->getUserDetailsById(AuthComponent::user('id')); ?>
  <div class="actions"> 
    <?php
        echo $this->Html->image(
            $userSession['User']['profile_picture'], 
            array(
                'width' => '56px',
                'height' => '56px'
            )
        ); 
    ?> 
    <h4> <?= $userSession['User']['first_name']; ?> <?= $userSession['User']['last_name']; ?>  </h4>
    <h5><b> @<?= $userSession['User']['username']; ?> <b></h5><br>
    <ul>
      <li><?= $this->HTML->link('Home', array('controller' => 'posts', 'action' => 'index')); ?></li>
      <li>
        <?= 
            $this->HTML->link(
                'My Profile', 
                array(
                    'controller' => 'users', 
                    'action' => 'view', 
                    AuthComponent::user('id')
                )
            ); 
        ?>
      </li>
      <li><?= $this->HTML->link('Log out', array('controller' => 'users', 'action' => 'logout')); ?></li>
    </ul>
  </div>
  
<div class="users form">
  <h2> Home </h2>
  <form method="post" role="search" action="/Search/index">
    <div class="form-group">
      <div class="row">
          <div class="input-group">
            <input name="details" id="details" type="text" class="form-control" placeholder="Search username or posts">
            <span class="input-group-btn">
              <button class="btn btn-primaryt" type="submit">Search</button>
            </span>
          </div>
      </div>
    </div>
  </form> 
  <hr style="border-top: 2px solid black;">
  <div>
    <?php echo $this->Form->create('Post'); ?>
    <div class="form-group">
      <div class="row">
          <?php 
              echo $this->Form->input(
                  'post_text',
                  array(
                      'required' => false,
                      'style' => 'resize: none;',
                      'class' => 'form-control',
                      'label' => '',
                      'placeholder' => 'Write your post here'
                  )
              ); 
          ?>
          <?php 
              $options = array(
                  'label' => 'Post',
                  'class' => 'btn btn-primary'
              );
              echo $this->Form->end($options); 
          ?>
      </div>
    </div>
  </div>
<?php else: ?> 
  <div>
<?php endif; ?>

  <div class="container-fluid">
    <?php include '../View/Posts/combinePost.ctp'; ?>
  </div>
</div>