<?php
    App::import('Controller', 'Likes');
    App::import('Controller', 'Retweets');
    App::import('Controller', 'Comments');
    App::import('Controller', 'Posts');
    App::import('Controller', 'Users');

    $likeCtrl = new LikesController();
    $retweetCtrl = new RetweetsController();
    $commentCtrl = new CommentsController();
    $postCtrl = new PostsController();
    $userCtrl = new UsersController();
    //die(print_r($this->request->data));
?>

<?php if (AuthComponent::user()) : ?>
  <?php $userSession = $userCtrl->getUserDetailsById(AuthComponent::user('id')); ?>
  <div class="actions"> 
    <?php
        echo $this->Html->image(
          $userSession['User']['profile_picture'], 
          array(
                'width' => '56px',
                'height' => '56px'
          )
        ); 
    ?> 
    <h4> <?= $userSession['User']['first_name']; ?> <?= $userSession['User']['last_name']; ?>  </h4>
    <h5><b> @<?= $userSession['User']['username']; ?> <b></h5><br>
    <ul>
      <li><?= $this->HTML->link('Home', array('controller' => 'posts', 'action' => 'index')); ?></li>
      <li>
        <?= 
            $this->HTML->link(
                'My Profile', 
                array(
                    'controller' => 'users', 
                    'action' => 'view', 
                    AuthComponent::user('id')
                )
            ); 
        ?>
      </li>
      <li><?= $this->HTML->link('Log out', array('controller' => 'users', 'action' => 'logout')); ?></li>
    </ul>
  </div>
<?php endif; ?>
<div class="users form">
<h1>Edit Post</h1>
<?php
    echo $this->Form->create(
        'Post', 
        array(
            'url' => array(
                'Controller' => 'posts',
                'action' => 'edit',
                $this->request->data['Post']['id'],
                $this->request->data['User']['id'],
                $this->request->data[0]
            )
        )
    );
    echo $this->Form->create('Post');
    echo $this->Form->input('post_text');
    echo $this->Form->end('Edit Post');
?>
</div>