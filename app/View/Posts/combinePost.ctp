<?php $retweets = $retweetCtrl->getAllRetweets(); ?>
<?php $lastArr = end($posts); ?>
<?php $arrPosts = array(); ?>

<?php foreach($posts as $post) : ?>
    <?php if ($post == $lastArr) : ?>
        <?php array_push($post, $post['Post']['created']); ?>
        <?php array_push($post, 0); ?>
        <?php array_push($arrPosts, $post); ?>
        <?php foreach($retweets as $retweet) : ?>
          <?php array_push($retweet, $retweet['Retweet']['created']); ?>
          <?php array_push($arrPosts, $retweet); ?>
        <?php endforeach; ?>
        <?php break; ?>
    <?php else : ?>
        <?php array_push($post, $post['Post']['created']); ?>
        <?php array_push($arrPosts, $post); ?>
    <?php endif; ?>
<?php endforeach; ?>

<?php
    usort($arrPosts, function($a, $b) {
        $a = strtotime($a["0"]);
        $b = strtotime($b["0"]);
        return $b - $a;
    });
?>

<?php foreach ($arrPosts as $post) : ?>

  <?php if (!empty($post['Retweet'])) : ?>
      <?php include '../View/Posts/repostPanel.ctp'; ?>
  <?php else : ?>
      <?php include '../View/Posts/postPanel.ctp'; ?>
  <?php endif; ?>

<?php endforeach; ?>

<?php if (count($arrPosts) >= 100) : ?>
  <?php/*
      echo $this->Paginator->counter(array(
        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
      ));
  ?>	</p>
  <div class="paging">
    <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </div>*/?>
<?php endif; ?>