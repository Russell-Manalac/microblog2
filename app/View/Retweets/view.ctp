
<?php
    App::import('Controller', 'Users');
    $userCtrl = new UsersController();
?>
<?php if (AuthComponent::user()) : ?>
  <?php $userSession = $userCtrl->getUserDetailsById(AuthComponent::user('id')); ?>
  <div class="actions"> 
    <?php
        echo $this->Html->image($userSession['User']['profile_picture'], array(
                'width' => '56px',
                'height' => '56px'
            )
        ); 
    ?> 
    <h4> <?= $userSession['User']['first_name']; ?> <?= $userSession['User']['last_name']; ?>  </h4>
    <h5><b> @<?= $userSession['User']['username']; ?> <b></h5><br>
    <ul>
      <li><?= $this->HTML->link('Home', array('controller' => 'posts', 'action' => 'index')); ?></li>
      <li><?= $this->HTML->link('Search', array('controller' => 'posts', 'action' => 'search')); ?></li>
      <li><?= $this->HTML->link('My Profile', array('controller' => 'users', 'action' => 'view', AuthComponent::user('id') )); ?></li>
      <li><?= $this->HTML->link('Log out', array('controller' => 'users', 'action' => 'logout')); ?></li>
    </ul>
  </div>
<?php endif; ?>

<div class="like view">
  <h3>
    <div class="row">
      <div class="col-md-4">
        Reposted by following users 
      </div>
    </div>
  </h3>
  <div>
    <div class="container-fluid">
      <?php foreach($retweets as $retweet) : ?>
        <?php if (!empty($retweet['Retweet'])) : ?>
          <?php if ($retweet['Retweet']['delete_state'] == 0) : ?>
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-1">
                    <?php
                        echo $this->Html->image($retweet['User']['profile_picture'], array(
                                'width' => '28px',
                                'height' => '28px'
                            )
                        ); 
                    ?>
                  </div>
                  <div class="col-md-2">
                    <p><b>
                  <?= 
                      $this->HTML->link(
                      $retweet['User']['first_name'].' '.$retweet['User']['last_name'], 
                      array('controller' => 'users', 'action' => 'view', $retweet['User']['id'])); 
                  ?>
					        </b></p>
                  </div>
                  <div class="col-md-3">
                    <p>@<?= $retweet['User']['username'] ?></p>
                  </div>
                </div>
              </div>
            </div>
          <?php endif; ?>
        <?php endif; ?>
      <?php endforeach; ?>
      <?php unset($likes); ?>
    </div>
  </div>  
  <?php
      echo $this->Paginator->counter(array(
        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
      ));
	?>	</p>
	<div class="paging">
    <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </div>
</div>
