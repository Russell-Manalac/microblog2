<div class="retweets index">
	<h2><?php echo __('Retweets'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('post_id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('delete_state'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($retweets as $retweet): ?>
	<tr>
		<td><?php echo h($retweet['Retweet']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($retweet['Post']['id'], array('controller' => 'posts', 'action' => 'view', $retweet['Post']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($retweet['User']['id'], array('controller' => 'users', 'action' => 'view', $retweet['User']['id'])); ?>
		</td>
		<td><?php echo h($retweet['Retweet']['delete_state']); ?>&nbsp;</td>
		<td><?php echo h($retweet['Retweet']['created']); ?>&nbsp;</td>
		<td><?php echo h($retweet['Retweet']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $retweet['Retweet']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $retweet['Retweet']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $retweet['Retweet']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $retweet['Retweet']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Retweet'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Posts'), array('controller' => 'posts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Post'), array('controller' => 'posts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
