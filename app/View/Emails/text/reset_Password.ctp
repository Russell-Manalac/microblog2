<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts.Email.text
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<?php echo $this->fetch('content'); ?>
Good day!,

In order to proceed with your password reset, kindly click this link below: 
		
http://localhost/Users/resetPassword/<?= $id ?>/<?= $activationCode ?>

http://dev6.ynsdev.pw//Users/resetPassword/<?= $id ?>/<?= $activationCode ?>
		
Thank you for your time,
Microblog Account Team.
