var generalTab = document.getElementById('generalTab');
var userTab = document.getElementById('userTab');
var postTab = document.getElementById('postTab');
var generalPanel = document.getElementById('generalPanel');
var userPanel = document.getElementById('usersPanel');
var postPanel = document.getElementById('postsPanel');



generalTab.addEventListener('click', displayAll);
userTab.addEventListener('click', displayPeople);
postTab.addEventListener('click', displayPosts);

generalPanel.style.display = "inline";
userPanel.style.display = "none";
postPanel.style.display = "none";

generalTab.classList.add('active');
userTab.classList.remove('active');
postTab.classList.remove('active');

function displayAll() {
    if (!generalTab.classList.contains('disabled')) {
        generalPanel.style.display = "inline";
        userPanel.style.display = "none";
        postPanel.style.display = "none";
    
        generalTab.classList.add('active');
        userTab.classList.remove('active');
        postTab.classList.remove('active');
    }
}

function displayPeople() {
    if (!userTab.classList.contains('disabled')) {
        generalPanel.style.display = "none";
        userPanel.style.display = "inline";
        postPanel.style.display = "none";

        generalTab.classList.remove('active');
        userTab.classList.add('active');
        postTab.classList.remove('active');
    }
}

function displayPosts() {
    if (!postTab.classList.contains('disabled')) {
        generalPanel.style.display = "none";
        userPanel.style.display = "none";
        postPanel.style.display = "inline";

        generalTab.classList.remove('active');
        userTab.classList.remove('active');
        postTab.classList.add('active');
    }
}