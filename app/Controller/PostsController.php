<?php

    class PostsController extends AppController 
    {

        public $components = array('Paginator', 'Session', 'Flash', 'Security', 'Redirect');

        public function index() 
        {
               $data = $this->Post->find(
                   'all',
                   array(
                       'order' => 'Post.created DESC'
                   )
                );
            $this->set('posts', $data);
            /* experiment sql for combining retweet posts and posts in a paginated page
            
            $sql = "SELECT `Post`.`id`, `Post`.`user_id`, `Post`.`post_text`, `Post`.`delete_state`, `Post`.`created`, `Post`.`modified`, `User`.`id`, `User`.`username`, `User`.`first_name`, `User`.`last_name`, `User`.`birth_date`, `User`.`password`, `User`.`profile_picture`, `User`.`email`, `User`.`activation_code`, `User`.`is_activated`, `User`.`disable_state`, `User`.`created`, `User`.`modified`,  `Retweets`.`id`, `Retweets`.`post_id`, `Retweets`.`user_id`, `Retweets`.`delete_state`, `Retweets`.`created`, `Retweets`.`modified` FROM `microblogdb`.`posts` AS `Post` LEFT JOIN `microblogdb`.`users` AS `User` ON (`Post`.`user_id` = `User`.`id`) LEFT JOIN `microblogdb`.`retweets` AS `Retweets` ON (`Post`.`id` = `Retweets`.`post_id`) WHERE 1 = 1 ORDER BY COALESCE(`Retweets`.`created`,`Post`.`created`), `Retweets`.`created` DESC";
            
            $this->set('posts', $this->Post->query($sql);
            */

            if ($this->request->is('post')) {
                $this->request->data['Post']['user_id'] = AuthComponent::user('id');
                return $this->redirect(array(
                    'action' => 'add', 
                    $this->request->data['Post']['post_text']
                ));
            }
        }


        public function add($postTxt) //add post from homepage
        {
            $this->request->data['Post']['post_text'] = $this->Security->sanitizeData($postTxt);
            $this->request->data['Post']['user_id'] = AuthComponent::user('id');
            $this->request->data['Post']['is_repost'] = 0;
            $this->request->data['Post']['delete_state'] = 0;
            $this->Post->create();
            if ($this->Post->save($this->request->data)) {
                $this->Session->setFlash('New post has been created!');
                $this->redirect('index');
            }
        }

        public function view($id) //view posts with comments paginated
        {
            $this->Paginator->settings = array(
                'conditions' => array(
                    'Comment.post_id =' => $id,
                    'Comment.delete_state !=' => 1,
                    'User.disable_state !=' => 1,
                ),
                'order' => 'Comment.created DESC',
                'limit' => 3
            );
            $paginatedData = $this->Paginator->paginate('Comment');
            if (!empty($paginatedData)) {
                $this->set('comments', $this->Paginator->paginate('Comment'));
            }
            if (empty($paginatedData)) {
                $data = $this->Post->findById($id);
                $this->set('comments', $data);
            }
        }

        public function viewRetweetedPostById($id) 
        {
            $data = $this->Post->findById($id);
            return $data;
        }

        public function getPostDetailsById($id) 
        {
            $data = $this->Post->findById($id['Comment']['post_id']);
            return $data;
        }

        public function retrievePossiblePostsByText($searchText) 
        {
            $options = array(
                'conditions' => array(
                    'OR' => array(
                        'Post.post_text LIKE' => '%'.$searchText['details'].'%'
                    )
                )
            );
            $data = $this->Post->find('all', $options);
            return $data;
        }

        public function edit($post_id, $user_id, $origin) //edit specific post
        {
            if (AuthComponent::user('id') == $user_id) {
                $data = $this->Post->findById($post_id);
                if ($this->request->is(array('post', 'put'))) {
                    $this->Post->id = $post_id;
                    $this->request->data['Post']['post_text'] = $this->Security->sanitizeData($this->request->data['Post']['post_text']);
                    if ($this->Post->save($this->request->data)) {
                        $this->Session->setFlash('The post has been edited');
                        $this->redirect($this->Redirect->redirectPostRequests($origin, $post_id, $user_id));
                    } else {
                        $this->Flash->error(__('Unable to update your post, please try again.'));
                        $this->redirect($this->Redirect->redirectPostRequests($origin, $post_id, $user_id));
                    }
                }
                array_push($data, $origin);
                $this->request->data = $data;
            } else {
                $this->Flash->error(__('You are not allowed to update posts you dont own.'));
                $this->redirect($this->Redirect->redirectPostRequests($origin, $post_id, $user_id));
            }

        }

        public function delete($post_id, $user_id, $origin) //soft delete post
        {
            //origin: 1 - deleteing from view post page | 2 - deleting from index post page | 3 - deleting from user page
            if (AuthComponent::user('id') == $user_id) {
                $options = array(
                    'conditions' => array(
                        'Post.id' => $post_id,
                        'Post.user_id' => $user_id
                    )
                );
                $this->request->data = $this->Post->find('first', $options);
                $this->request->data['Post']['delete_state'] = 1;

                if ($this->request->is(array('post', 'put'))) {
                    if ($this->Post->save($this->request->data)) {
                        $this->Flash->error('Your post has been deleted.');
                        $this->redirect($this->Redirect->redirectPostRequests($origin, $post_id, $user_id));
                    } else {
                        $this->Flash->error(__('Unable to delete your post, please try again.'));
                        $this->redirect($this->Redirect->redirectPostRequests($origin, $post_id, $user_id));
                    }
                } else {
                    $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
                    $this->request->data = $this->User->find('first', $options);
                }
            } else {
                $this->Flash->error(__('You are not allowed to delete posts you dont own.'));
                $this->redirect($this->Redirect->redirectPostRequests($origin, $post_id, $user_id));
            }

        }

    }