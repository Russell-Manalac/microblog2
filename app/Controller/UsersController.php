<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController 
{

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Session', 'Flash', 'Register', 'Security', 'Redirect');

    public function beforeFilter() 
    {
        $this->Auth->allow(
            'add', 
            'generateActivationEmail', 
            'activateAccount', 
            'generateReactivationEmail', 
            'reactivateAccount',
            'sendResetRequest',
            'resetPassword',
            'renewPassword'
        );
    }

/**
 * index method
 *
 * @return void
 */
    //Login with auto reactivation if an account is deactivated
    public function login() 
    {
        if ($this->request->is('post')) {
            $this->request->data['User']['username'] = $this->Security->sanitizeData($this->request->data['User']['username']);
            $this->request->data['User']['password'] = $this->Security->sanitizeData($this->request->data['User']['password']);
            $options = array(
                'conditions' => array(
                    'User.password' => AuthComponent::password($this->request->data['User']['password']),
                    'User.username' => $this->request->data['User']['username']
                )
            );
            $datum = $this->User->find('first', $options);
            if (!empty($datum)) {
                if ($datum['User']['is_activated'] == 1) {
                    if ($datum['User']['disable_state'] == 0) {
                        if ($this->Auth->login()) {
                            return $this->redirect($this->Auth->redirectUrl());
                            $this->Session->setFlash('test');
                        } else {
                            $this->Session->setFlash('Failed to login');
                        }
                    } else {
                        //die(print_r($datum['User']));
                        $uri = 'generateReactivationEmail/'.
                        $datum['User']['id'];
                        return $this->redirect($uri);
                        /*
                        return $this->redirect(
                            array(
                                'action' => 'generateReactivationEmail',
                                $datum['User']['id']
                            )
                        );*/
                    }
                } else {
                    $this->Session->setFlash('Account not activated, kindly check your email for an activation code.');
                }
            } else {
                $this->Session->setFlash('Invalid Username or Password');
                return $this->redirect('login');
            }
        }
    }

    //For new registers
    public function activateAccount($id, $userCode) 
    {
        $options = array(
            'conditions' => array(
                'User.activation_code' => $userCode
            )
        );
        $this->request->data = $this->User->find('first', $options);
        if ($userCode == $this->request->data['User']['activation_code']) {
            $this->request->data['User']['is_activated'] = 1;
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('Account activated, you may now login.'));
                
            return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        } else {
            $this->Flash->error(__('Invalid Activation Link Please Try again'));
        }
    }
    
    public function generateActivationEmail($id, $regEmail, $userCode) 
    {
        $email = new CakeEmail('gmail');
        $email->from('russelljacobmanalac.yns@gmail.com');
        $email->to($regEmail); //'rjmanalac.testmail@gmail.com'
        $email->subject('Email Verification');
        $email->viewVars(
            array(
                'id' => $id,
                'activationCode' => $userCode
            )
        );
        $email->template('activate');
        debug($email->send());
        
            return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
    }

    //For Disabled Accounts
    public function generateReactivationEmail($id) 
    {
        $options = array('conditions' => array('User.id' => $id));
        $this->request->data = $this->User->find('first', $options);
        $this->request->data['User']['activation_code'] = $this->Register->generateActivationCode();
        if ($this->User->save($this->request->data)) {
            $email = new CakeEmail('gmail');
            $email->from('russelljacobmanalac.yns@gmail.com');
            $email->to($this->request->data['User']['email']); //'rjmanalac.testmail@gmail.com'
            $email->subject('Account Reactivation');
            $email->viewVars(array(
                'id' => $this->request->data['User']['id'],
                'activationCode' => $this->request->data['User']['activation_code']
            ));
            $email->template('reactivate');
            debug($email->send());
            $this->Flash->success(__('Account reactivation has been sent kindly check your inbox'));
            
            return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
        } else {
            $this->Flash->error(__('Unable to send the confirmation email. Please, try again.'));
            
            return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
        }
    }

    public function reactivateAccount($id, $userCode) 
    {
        $options = array(
            'conditions' => array(
                'User.id' => $id,
                'User.activation_code' => $userCode
            )
        );
        $this->request->data = $this->User->find('first', $options);
        if ($userCode == $this->request->data['User']['activation_code']) {
            $this->request->data['User']['disable_state'] = 0;
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('Account reactivated, you may now login.'));
                return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
            } else {
                $this->Flash->error(__('The user could not be activated. Please, try again.'));
                return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
            }
        } else {
            $this->Flash->error(__('Invalid Activation Link Please Try again'));
            return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
        }
    }

    public function logout() 
    {
        $this->Auth->logout();
        $this->redirect( '/posts/index' );
    }

    public function index() 
    {
        return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id = null) //view specific user
    {
        $totalFollowing = $totalFollowers = 0;
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array(
            'conditions' => array('User.' . $this->User->primaryKey => $id),
            //'order' => 'User.post.created DESC'
        );
        $data = $this->User->find('first', $options);
        $options = array(
            'conditions' => array(
                'Follow.follower_id =' => $id 
            )
        );
        $totalFollowing = $this->User->Follow->find('count', $options);
        $options = array(
            'conditions' => array(
                'Follow.user_id =' => $id,
                'Follow.unfollow_state !=' => 1,
            )
        );
        $totalFollowers = $this->User->Follow->find('count', $options);
        array_push($data, $totalFollowing, $totalFollowers);
        $this->set('user', $data);
    }

    public function getUserDetailsById($id)
    {
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $data = $this->User->find('first', $options);
        return $data;
    }

    public function retrievePossibleUsersByUsername($searchText) //for search feature
    {
        $options = array(
            'conditions' => array(
                'OR' => array(
                    'User.username LIKE' => '%'.$searchText['details'].'%'
                )
            )
        );
        $data = $this->User->find('all', $options);
        //$data = $this->User->query('SELECT * FROM users WHERE username LIKE "%'.$searchText['details'].'%";');
        return $data;
    }

/**
 * add method
 *
 * @return void
 */

    public function add() //register new user
    {
        if (empty(AuthComponent::user())) {
            if ($this->request->is('post')) {
                $this->request->data['User']['username'] = $this->Security->sanitizeData(
                    $this->request->data['User']['username']
                );
                $this->request->data['User']['first_name'] = $this->Security->sanitizeData(
                    $this->request->data['User']['first_name']
                );
                $this->request->data['User']['last_name'] = $this->Security->sanitizeData(
                    $this->request->data['User']['last_name']
                );
                $this->request->data['User']['email'] = $this->Security->sanitizeData(
                    $this->request->data['User']['email']
                );
                $this->request->data['User']['disable_state'] = 0;
                $this->request->data['User']['is_activated'] = 0;
                $this->request->data['User']['activation_code'] = $this->Security->sanitizeData(
                    $this->Register->generateActivationCode()
                );
                $this->request->data['User']['profile_picture'] = "360_F_64676383_LdbmhiNM6Ypzb3FM4PPuFP9rHe7ri8Ju.jpg";
                $this->User->create();
                if ($this->User->save($this->request->data)) {
                    $this->request->data['User']['password'] = $this->Security->sanitizeData(
                        AuthComponent::password($this->request->data['User']['password'])
                    );
                    $this->User->save($this->request->data);
                    //die(print_r($this->request->data));

                    $this->Flash->success(__('The user has been saved. Kindly Check your email first for verification'));

                    $options = array(
                        'conditions' => array(
                            'User.password' => $this->request->data['User']['password'],
                            'User.username' => $this->request->data['User']['username'],
                            'User.email' => $this->request->data['User']['email'],
                            'User.activation_code' => $this->request->data['User']['activation_code']
                        )
                    );
                    $data = $this->User->find('first', $options);

                    $uri = 'generateActivationEmail/'.
                    $data['User']['id'].
                    '/'.$data['User']['email'].
                    '/'.$data['User']['activation_code'];
                    return $this->redirect($uri);

                } else {
                    $this->Flash->error(__('Failed to Register new account. Please, try again.'));
                }
            } 
        } else {
            return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
        }
    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) //edit user credentials
    {
        if (AuthComponent::user('id') == $id) {
            if (!$this->User->exists($id)) {
                throw new NotFoundException(__('Invalid user'));
            }
            if ($this->request->is(array('post', 'put'))) {
                $this->request->data['User']['username'] = $this->Security->sanitizeData(
                    $this->request->data['User']['username']
                );
                $this->request->data['User']['first_name'] = $this->Security->sanitizeData(
                    $this->request->data['User']['first_name']
                );
                $this->request->data['User']['last_name'] = $this->Security->sanitizeData(
                    $this->request->data['User']['last_name']
                );
                $this->request->data['User']['email'] = $this->Security->sanitizeData(
                    $this->request->data['User']['email']
                );
                if (!empty($this->request->data['User']['profile_picture']['name'])) {
                    $this->request->data['User']['profile_picture']['name'] = $this->Security->sanitizeData(
                        $this->request->data['User']['profile_picture']['name']
                    );
                    $file = $this->request->data['User']['profile_picture']; // Creating a variable to handle upload
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //processing file extension

                    if(in_array($ext, $arr_ext))
                    {
                        move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);
                        $this->request->data['User']['profile_picture'] = $file['name'];
                    }
                } else {
                    unset($this->request->data['User']['profile_picture']);
                }
                if ($this->User->save($this->request->data)) {

                    $this->Flash->success(__('your user details has been saved.'));
                    return $this->redirect(array('action' => 'Edit', $id));
                } else {
                    $this->Flash->error(__('user details could not be saved. Please, try again.'));
                }
            } else {
                $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
                $this->request->data = $this->User->find('first', $options);
            }
        } else {
            $this->Flash->error(__('You are not allowed to edit this profile.'));
            return $this->redirect(array('action' => 'View', $id));
        }
    }

    public function sendResetRequest() //send password request link
    {
        if ($this->request->is(array('post'))) {
            //die(print_r($this->request->data));
            $options = array(
                'conditions' => array(
                    'username' => $this->request->data['User']['username'],
                    'email' => $this->request->data['User']['email']
                )
            );
            $this->request->data = $this->User->find('first', $options);
            $this->request->data['User']['activation_code'] = $this->Register->generateActivationCode();
            if ($this->User->save($this->request->data)) {
                $email = new CakeEmail('gmail');
                $email->from('russelljacobmanalac.yns@gmail.com');
                $email->to($this->request->data['User']['email']); //'rjmanalac.testmail@gmail.com'
                $email->subject('Account Reactivation');
                $email->viewVars(array(
                    'id' => $this->request->data['User']['id'],
                    'activationCode' => $this->request->data['User']['activation_code']
                ));
                $email->template('resetPassword');
                debug($email->send());
                $this->Flash->success(__('Password reset request has been sent kindly check your inbox'));
                return $this->redirect(array('action' => 'sendResetRequest'));
            } else {
                $this->Flash->error(__('Unable to send the confirmation email. Please, try again.'));
                return $this->redirect(array('action' => 'sendResetRequest'));
            }
        }
    }

    public function resetPassword($id, $userCode) //redirect user to the resetpassword page with data from the requester
    {
        $options = array(
            'conditions' => array(
                'User.id' => $id,
                'User.activation_code' => $userCode
            )
        );
        $this->request->data = $this->User->find('first', $options);
        if ($userCode == $this->request->data['User']['activation_code']) {
            $this->set('userDetails', $this->request->data);
        } else {
            $this->Flash->error(__('Invalid password reset Link Please Try again'));
            return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
        }
    }

    public function renewPassword($type, $userCode, $id = null) //renew password of the requester
    {
        //Type: 1 - password reset | 2 - edit password
        $newPassword = $this->Security->sanitizeData(
            $this->request->data['User']['new_Password']
        );
        $confirmPassword = $this->Security->sanitizeData(
            $this->request->data['User']['confirm_Password']
        );
        if ($newPassword == $confirmPassword) {
            $options = array(
                'conditions' => array(
                    'User.id' => $id,
                    'User.activation_code' => $userCode
                )
            );
            $this->request->data = $this->User->find('first', $options);

            if ($this->request->is(array('post', 'put'))) {
                $this->request->data['User']['password'] = AuthComponent::password($newPassword);
                if ($this->User->save($this->request->data)) {
                    $this->Flash->success(__('your password has been saved.'));
                    return $this->redirect($this->Redirect->redirectUserRequests($type, $id));
                } else {
                    $this->Flash->error(__('password could not be saved. Please, try again.'));
                    return $this->redirect($this->Redirect->redirectUserRequests($type, $id));
                }
            } else {
                $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
                $this->request->data = $this->User->find('first', $options);
            }
        } else {
            $this->Flash->error(__('Please confirm your password correctly align with the new password.'));
            return $this->redirect($this->Redirect->redirectUserRequests($type, $id));
        }
    }

    public function editPassword($type, $id = null) //edit password from edit profile
    {
        //Type: 1 - password reset | 2 - edit password
        
        if (AuthComponent::user('id') == $id) {
            $newPassword = $this->Security->sanitizeData(
                $this->request->data['User']['new_Password']
            );
            $confirmPassword = $this->Security->sanitizeData(
                $this->request->data['User']['confirm_Password']
            );
            if ($newPassword == $confirmPassword) {
                $options = array(
                    'conditions' => array(
                        'User.id' => $id
                    )
                );
                $oldPassword = $this->Security->sanitizeData(
                    $this->request->data['User']['old_Password']
                );
                $this->request->data = $this->User->find('first', $options);
    
                if ($this->request->is(array('post', 'put'))) {
                    if (AuthComponent::password($oldPassword) != $this->request->data['User']['password']) {
                        $this->Flash->error(__('invalid current password. unable to proceed with change password.'));
                        return $this->redirect($this->Redirect->redirectUserRequests($type, $id));
                    } else {
                        $this->request->data['User']['password'] = AuthComponent::password($newPassword);
                        if ($this->User->save($this->request->data)) {
                            $this->Flash->success(__('your password has been saved.'));
                            return $this->redirect($this->Redirect->redirectUserRequests($type, $id));
                        } else {
                            $this->Flash->error(__('password could not be saved. Please, try again.'));
                            return $this->redirect($this->Redirect->redirectUserRequests($type, $id));
                        }
                    }
                } else {
                    $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
                    $this->request->data = $this->User->find('first', $options);
                }
            } else {
                $this->Flash->error(__('Please confirm your password correctly align with the new password.'));
                return $this->redirect($this->Redirect->redirectUserRequests($type, $id));
            }
        
        } else {
            $this->Flash->error(__('You are not allowed to edit this profile.'));
            return $this->redirect($this->Redirect->redirectUserRequests($type, $id));
        }
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) //disable account
    {
        if (AuthComponent::user('id') == $id) {
            $options = array(
                'conditions' => array(
                    'User.id' => $id
                )
            );
            $this->request->data = $this->User->find('first', $options);
            $this->request->data['User']['disable_state'] = 1;

            if ($this->request->is(array('post', 'put'))) {
                if ($this->User->save($this->request->data)) {
                    $this->Flash->error('Your account has been deactivated.');
                    $this->Auth->logout();
                    return $this->redirect('index');	
                } else {
                    $this->Flash->error(__('Unable to deactivate your Account, Please, try again.'));
                }
            } else {
                $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
                $this->request->data = $this->User->find('first', $options);
            }
        } else {
            $this->Flash->error(__('You are not allowed to delete this profile.'));
            return $this->redirect(array('action' => 'View', $id));	
        }
    }

}
