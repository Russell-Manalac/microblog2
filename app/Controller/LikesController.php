<?php
App::uses('AppController', 'Controller');
/**
 * Likes Controller
 *
 * @property Like $Like
 * @property PaginatorComponent $Paginator
 */
class LikesController extends AppController 
{

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Session', 'Flash', 'Security');

/**
 * index method
 *
 * @return void
 */
    /*
    public function index() 
    {
        $this->Like->recursive = 0;
        $this->set('likes', $this->Paginator->paginate());
    }
    */

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id) //view likers
    {
        $this->Paginator->settings = array(
            'conditions' => array(
                'Like.post_id =' => $id,
                'Like.unlike_state !=' => 1,
                'Liker.disable_state !=' => 1
            ),
            'limit' => 10
        );
        $this->set('likes', $this->Paginator->paginate());
    }

    public function getCurrentLikesById($post_id) //get total count of likes in a post
    {
        $options = array(
            'conditions' => array(
                'Like.post_id =' => $post_id,
                'Like.unlike_state !=' => 1,
                'Liker.disable_state !=' => 1
            )
        );
        $totalLikes = $this->Like->find('count', $options);
        return $totalLikes;
    }

/**
 * add method
 *
 * @return void
 */
    //like a post, if it was already unliked it will just update the exisitng data
    public function add($post_id, $user_id)
    {
        if (AuthComponent::user('id') == $user_id) {
            $options = array('conditions' => array(
                'Like.post_id' => $post_id,
                'Like.liker_id' => $user_id
            ));
            $this->request->data = $this->Like->find('first', $options);
            $this->request->data['Like']['post_id'] = $post_id;
            $this->request->data['Like']['liker_id'] = $user_id;
            $this->request->data['Like']['unlike_state'] = 0;
            if (empty($this->request->data)) {
                $this->Follow->create();
            }
            if ($this->Like->save($this->request->data)) {
                $this->Flash->success(__('You have liked this post.'));
                return $this->redirect(array(
                    'controller' => 'posts',
                    'action' => 'view',
                    $post_id
                    )
                );
            } else {
                $this->Flash->error(__('Unable to like post. Please, try again.'));
            }
        } else {
            $this->Flash->error(__('You are not allowed to like a post as a different user.'));
            return $this->redirect(array('controller' => 'posts', 'action' => 'View', $post_id));
        }
    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    /*
    public function edit($id = null) 
    {
        if (AuthComponent::user('id') == $user_id) {
            if (!$this->Like->exists($id)) {
                throw new NotFoundException(__('Invalid like'));
            }
            if ($this->request->is(array('post', 'put'))) {
                if ($this->Like->save($this->request->data)) {
                    $this->Flash->success(__('The like has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Flash->error(__('The like could not be saved. Please, try again.'));
                }
            } else {
                $options = array('conditions' => array('Like.' . $this->Like->primaryKey => $id));
                $this->request->data = $this->Like->find('first', $options);
            }
            $posts = $this->Like->Post->find('list');
            $users = $this->Like->User->find('list');
            $this->set(compact('posts', 'users'));
        } else {
            $this->Flash->error(__('You are not allowed to edit other user likes.'));
            return $this->redirect(array('controller' => 'posts', 'action' => 'View', $post_id));
        }
    }
    */

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($post_id, $user_id) //soft delete like
    {
        if (AuthComponent::user('id') == $user_id) {
            $options = array('conditions' => array(
                'Like.post_id' => $post_id,
                'Like.liker_id' => $user_id
            ));
            $this->request->data = $this->Like->find('first', $options);
            $this->request->data['Like']['unlike_state'] = 1;
            
            if ($this->Like->save($this->request->data)) {
                $this->Flash->error(__('you have unliked the post.'));
                return $this->redirect(array(
                    'controller' => 'posts',
                    'action' => 'view',
                    $post_id
                    )
                );
            } else {
                $this->Flash->error(__('Unable to unlike post. Please, try again.'));
            }
        } else {
            $this->Flash->error(__('You are not allowed to unlike other likes.'));
            return $this->redirect(array('controller' => 'posts', 'action' => 'View', $post_id));
        }
    }
}
