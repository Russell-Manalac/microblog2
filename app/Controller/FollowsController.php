<?php
App::uses('AppController', 'Controller');
/**
 * Follows Controller
 *
 * @property Follow $Follow
 * @property PaginatorComponent $Paginator
 */
class FollowsController extends AppController 
{

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Session', 'Flash', 'Security');

/**
 * index method
 *
 * @return void
 */
    /*
    public function index() 
    {
        $this->Follow->recursive = 0;
        $this->set('follows', $this->Paginator->paginate());
    }
    */

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    //view Who's following the user/Who's the user following
    public function view($id = null, $type) 
    {
        if ($type == 0) { //if type of display is for list of following
            $options = array(
                'conditions' => array(
                    'Follow.follower_id =' => $id 
                )
            );
            $data = $this->Follow->find('all', $options);
        }
        if ($type == 1) { //if type of display is for list of followers
            $options = array(
                'conditions' => array(
                    'Follow.user_id =' => $id,
                    'Follow.unfollow_state !=' => 1,
                )
            );
            $data = $this->Follow->find('all', $options);
        }
        array_push($data, $type);
        $this->set('follows', $data);
    }

/**
 * add method
 *
 * @return void
 */
    //follow user, if its was already unfollowed it should just update the unfollow_state to 0
    public function add($user_id, $follower_id, $username) 
    {
        $options = array(
            'conditions' => array(
                'Follow.user_id' => $user_id,
                'Follow.follower_id' => $follower_id
            )
        );
        $this->request->data = $this->Follow->find('first', $options);
        if (empty($this->request->data)) {
            $this->Follow->create();
        }
        $this->request->data['Follow']['user_id'] = $this->Security->sanitizeData($user_id);
        $this->request->data['Follow']['follower_id'] = $this->Security->sanitizeData($follower_id);
        $this->request->data['Follow']['unfollow_state'] = 0;
        if ($this->Follow->save($this->request->data)) {
            $this->Flash->success(__('Followed ' . $username));
            return $this->redirect(array('controller' => 'users', 'action' => 'view', $user_id));
        } else {
            $this->Flash->error(__('unable to follow '.$username.'. Please, try again.'));
        }
    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    /*
    public function edit($id = null) 
    {
        if (!$this->Follow->exists($id)) {
            throw new NotFoundException(__('Invalid follow'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Follow->save($this->request->data)) {
                $this->Flash->success(__('The follow has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The follow could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Follow.' . $this->Follow->primaryKey => $id));
            $this->request->data = $this->Follow->find('first', $options);
        }
        $users = $this->Follow->User->find('list');
        $this->set(compact('users'));
    }
    */

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($user_id, $follower_id, $username) //unfollow user
    {
        $options = array(
            'conditions' => array(
                'Follow.user_id' => $user_id,
                'Follow.follower_id' => $follower_id
            )
        );
        $this->request->data = $this->Follow->find('first', $options);
        $this->request->data['Follow']['unfollow_state'] = 1;
        if ($this->Follow->save($this->request->data)) {
            $this->Flash->error(__('Unfollowed ' . $username));
            return $this->redirect(array('controller' => 'users', 'action' => 'view', $user_id));
        } else {
            $this->Flash->error(__('Unable to unfollow'.$username.'. Please, try again.'));
        }
    }
}
