<?php
App::uses('AppController', 'Controller');
/**
 * Retweets Controller
 *
 * @property Retweet $Retweet
 * @property PaginatorComponent $Paginator
 */
class RetweetsController extends AppController 
{

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Session', 'Flash', 'Security', 'Redirect');

/**
 * index method
 *
 * @return void
 */
    /*
    public function index() 
    {
        $this->Paginator->settings = array(
            'limit' => 3
        );
        $this->Retweet->recursive = 0;
        $this->set('retweets', $this->Paginator->paginate());
    }
    */

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id) //view the following reposters
    {
        $this->Paginator->settings = array(
            'conditions' => array(
                'Retweet.post_id =' => $id,
                'Retweet.delete_state !=' => 1,
            ),
            'group' => array('Retweet.user_id'),
            'limit' => 10
        );
        $this->set('retweets', $this->Paginator->paginate());
    }

    public function getRetweetCountById($id) //Get repost counts base on selected post
    {
        $options = array(
            'conditions' => array(
                'Retweet.post_id =' => $id,
                'Retweet.delete_state !=' => 1,
                'User.disable_state !=' => 1
            )
        );
        $totalRetweets = $this->Retweet->find('count', $options);
        return $totalRetweets;
    }

    public function getAllRetweets() 
    {
        $options = array(
            'order' => 'Retweet.created DESC'
        );
        return $this->Retweet->find('all', $options);
    }

    public function getRetweetsById($id) 
    {
        $options = array(
            'conditions' => array(
                'Retweet.user_id =' => $id,
                'Retweet.delete_state !=' => 1,
            ),
            'order' => 'Retweet.created DESC'
        );
        return $this->Retweet->find('all', $options);
    }

/**
 * add method
 *
 * @return void
 */
    public function add($post_id, $user_id) //Repost post 
    {
        if (AuthComponent::user('id') == $user_id) {
            $this->request->data['Retweet']['post_id'] = $this->Security->sanitizeData($post_id);
            $this->request->data['Retweet']['user_id'] = $this->Security->sanitizeData($user_id);
            $this->request->data['Retweet']['delete_state'] = 0;
            $this->Retweet->create();
            if ($this->Retweet->save($this->request->data)) {
                $this->Flash->success(__('You reposted this post.'));
                return $this->redirect(array(
                        'controller' => 'posts',
                        'action' => 'view',
                        $post_id
                    )
                );
            } else {
                $this->Flash->error(__('Unable to repost. Please, try again.'));
            }
        } else {
            $this->Flash->error(__('You are not allowed to retweet under different user.'));
            return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
        }
    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    /*
    public function edit($id = null) 
    {
        if (!$this->Retweet->exists($id)) {
            throw new NotFoundException(__('Invalid retweet'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Retweet->save($this->request->data)) {
                $this->Flash->success(__('The retweet has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The retweet could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Retweet.' . $this->Retweet->primaryKey => $id));
            $this->request->data = $this->Retweet->find('first', $options);
        }
        $posts = $this->Retweet->Post->find('list');
        $users = $this->Retweet->User->find('list');
        $this->set(compact('posts', 'users'));
    }
    */

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */     
    public function delete($id, $post_id, $user_id, $origin) //soft delete repost
    {
        if (AuthComponent::user('id') == $user_id) {
            $options = array(
                'conditions' => array(
                    'Retweet.id' => $id,
                    'Retweet.post_id' => $post_id,
                    'Retweet.user_id' => $user_id
                )
            );
            $this->request->data = $this->Retweet->find('first', $options);
            if ($this->request->is(array('post', 'put'))) {
                $this->request->data['Retweet']['delete_state'] = 1;
                if ($this->Retweet->save($this->request->data)) {
                    $this->Flash->error('The reposted post has been deleted.');
                    $this->redirect($this->Redirect->redirectPostRequests($origin, $post_id, $user_id));
                } else {
                    $this->Flash->error(__('Reposted post could not be deleted. Please, try again.'));
                    $this->redirect($this->Redirect->redirectPostRequests($origin, $post_id, $user_id));
                }
            } else {
                $options = array('conditions' => array('Retweet.' . $this->Retweet->primaryKey => $id));
                $this->request->data = $this->Retweet->find('first', $options);
            }
            $this->redirect($this->Redirect->redirectPostRequests($origin, $post_id, $user_id));
        } else {
            $this->Flash->error(__('You are not allowed to delete retweets of other users.'));
            $this->redirect($this->Redirect->redirectPostRequests($origin, $post_id, $user_id));
        }
    }
}
