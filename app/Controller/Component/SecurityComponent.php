<?php
App::uses('Component', 'Controller');

class SecurityComponent extends Component 
{
    
    //XSS
    public function sanitizeData($data) //XSS Prevention
    {
        if (!empty($data)) {
            return h($data, ENT_QUOTES, 'UTF-8');
        } else {
            return $data;
        }
    }
    
    //CSRF
}