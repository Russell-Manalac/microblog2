<?php
App::uses('Component', 'Controller');

class RedirectComponent extends Component 
{
    public function redirectPostRequests($origin, $post_id, $user_id) //it manages the redirection from post controller
    {
        //$origin: 1 - deleteing from view post page($post_id) | 2 - deleting from index post page(no $id) | 3 - deleting from user page($user_id)
        switch ($origin) {
            case 1:
                return array(
                    'controller' => 'posts',
                    'action' => 'view', 
                    $post_id
                );
                break;
            case 2:
                return array(
                    'controller' => 'posts',
                    'action' => 'index'
                );
                break;
            case 3:
                return array(
                    'controller' => 'users',
                    'action' => 'view',
                    $user_id
                );
                break;
            default:
                return array(
                    'controller' => 'posts',
                    'action' => 'index'
                );
                break;
        }

    }

    public function redirectUserRequests($origin, $user_id) //it manages the redirection from user controller
    {
        //$origin: 1 - updating password from forgot password | 2 - updating password from edit page 
        switch ($origin) {
            case 1:
                return array(
                    'controller' => 'posts',
                    'action' => 'index'
                );
                break;
            case 2:
                return array(
                    'controller' => 'users',
                    'action' => 'edit',
                    $user_id
                );
                break;
            default:
                return array(
                    'controller' => 'posts',
                    'action' => 'index'
                );
                break;
        }

    }
}