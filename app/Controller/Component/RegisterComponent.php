<?php
App::uses('Component', 'Controller');

class RegisterComponent extends Component 
{
    
    public function generateActivationCode() //it helps generate activation code, and to decrease line usage in users controller
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $code = substr(str_shuffle($permitted_chars), 0, 10);
        return $code;
    }
}