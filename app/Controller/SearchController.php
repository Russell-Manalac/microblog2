<?php
App::uses('AppController', 'Controller');

    class SearchController extends AppController 
    {
	    public $components = array('Session', 'Security');
        public function index() //redirect to search page
        {
            $this->set('detail', $this->Security->sanitizeData($this->request->data));
        }    

    }