<?php

    class CommentsController extends AppController 
    {

        public $components = array('Session', 'Flash', 'Security');

        /*
        public function index() 
        {
            $data = $this->Comment->find('all');
            $this->set('comments', $data);
        }
        */

        public function add($post_id, $user_id) //Add comment in a specific post
        {
            if (AuthComponent::user('id') == $user_id) {
                $this->request->data['Comment']['comment_text'] = $this->Security->sanitizeData(
                    $this->request->data['Comment']['comment_text']
                );
                $this->request->data['Comment']['post_id'] = $post_id;
                $this->request->data['Comment']['user_id'] = $user_id;
                $this->request->data['Comment']['delete_state'] = 0;

                $this->Comment->create();
                if ($this->Comment->save($this->request->data)) {
                    $this->Session->setFlash('The comment has been created!');
                    $this->redirect(array(
                        'controller' => 'posts',
                        'action' => 'view',
                        $post_id
                    ));
                }
            } else {
                $this->Flash->error(__('You are not allowed to comment as a different user.'));
                return $this->redirect(array('controller' => 'posts', 'action' => 'View', $post_id));
            }
        }

        /*
        public function view($id) 
        {
            $data = $this->Comment->findById($id);
            $this->set('comment', $data);
        }
        */

        public function getCommentCountById($id) //Get total count of comments in a specific post
        {
            $options = array(
                'conditions' => array(
                    'Comment.post_id =' => $id,
                    'Comment.delete_state !=' => 1,
                    'User.disable_state !=' => 1
                )
            );
            $totalComments = $this->Comment->find('count', $options);
            return $totalComments;
        }

        public function delete($id, $post_id, $user_id) //Soft delete comment
        {
            if (AuthComponent::user('id') == $user_id) {
                $options = array(
                    'conditions' => array(
                        'Comment.id' => $id,
                        'Comment.post_id' => $post_id,
                        'Comment.user_id' => $user_id
                    )
                );
                $this->request->data = $this->Comment->find('first', $options);
                $this->request->data['Comment']['delete_state'] = 1;
                
                if ($this->Comment->save($this->request->data)) {
                    $this->Flash->success(__('you have deleted your comment in this post.'));
                    return $this->redirect(
                        array(
                            'controller' => 'posts',
                            'action' => 'view',
                            $post_id
                        )
                    );
                } else {
                    $this->Flash->error(__('Unable to delete comment. Please, try again.'));
                    return $this->redirect(
                        array(
                            'controller' => 'posts',
                            'action' => 'view',
                            $post_id
                        )
                    );
                }
            } else {
                $this->Flash->error(__('You are not allowed to delete this comment.'));
                return $this->redirect(
                    array(
                        'controller' => 'posts',
                        'action' => 'view',
                        $post_id
                    )
                );
            }
        }
    }